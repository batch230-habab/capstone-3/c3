import { Button, Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import userContext from '../UserContext'
import Swal from 'sweetalert2';
import './ProductsCard.css';

export default function ProductsCard({ProductsProp}){
    const {_id, prodName, prodDesc, price, isAvailable, stockAmount, orders} = ProductsProp;
    const [productId, setProductId] = useState(ProductsProp._id)
    useEffect(() =>{
        
    })


    //?Add to cart
    function addToCart(){
        fetch(`http://localhost:4000/users/addToCart`,{
            method: 'POST',
            body: JSON.stringify({productId}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.text())
        .then(data =>{
            (localStorage.token !== undefined)?
            Swal.fire({
                title:data
            })
            :
            Swal.fire({
                title: 'Log in to purchase'
            })
            .then(()=>{window.location="/login"})
        })
    }

    //?View in admin dashboard
    function viewProduct(){
        fetch(`http://localhost:4000/products/${_id}`)
        .then(res=>res.text())
        .then(data=>{
            console.log(data)
            Swal.fire({
                title: 'View in admin dashboard',
                showConfirmButton: true
            })
            .then(()=>{
                window.location = "/admindashboard";
            })
        })
        
    }
    return (
        <Card className="m-4 col-lg-3 col-md-8 col-sm-10 d-flex" id="productsCard">
            <Card.Body>
                <Card.Title>{prodName}</Card.Title>
                <Card.Text>{prodDesc}</Card.Text>
                <Card.Text>Price: ₱{price}</Card.Text>
                <Card.Text>Stock: {stockAmount}</Card.Text>
                <Button variant = "secondary" style={{position:'absolute', right:'20px', top: '5rem'}} onClick={viewProduct} className="btn-sm" hidden>View Product</Button>
                {
                    (localStorage.isAdmin=="true")?<Button variant = "secondary" style={{position:'absolute', right:'20px', top: '5rem'}} onClick={viewProduct} className="btn-sm">View Product</Button>:
                    <Card.Text></Card.Text>
                }
                <Button variant = "primary" style={{position:'absolute', right:'20px', top:'8rem'}} onClick={addToCart} className="btn-sm">Add to Cart</Button>
            </Card.Body>
        </Card>
    )
}

ProductsCard.propTypes = {
    ProductsProp: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        prodName: PropTypes.string.isRequired,
        prodDesc: PropTypes.string,
        price: PropTypes.number.isRequired,
        isAvailable: PropTypes.bool.isRequired,
        stockAmount: PropTypes.number.isRequired,
        orders: PropTypes.array.isRequired
    })
}