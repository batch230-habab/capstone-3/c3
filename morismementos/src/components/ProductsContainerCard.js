import {useEffect, useState} from 'react';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import EditProduct from './EditProductModal';
import Swal from 'sweetalert2';
import './ProductsContainerCard.css'
import { useLocation } from 'react-router';


export default function ProductsContainerCard({ProductsContainerCardProp}) {
    const{_id, prodName, prodDesc, price, isAvailable, stockAmount, orders} = ProductsContainerCardProp;

    const [EditProductProp, setEditProductProp] = useState(ProductsContainerCardProp);

    useEffect(() =>{
        setEditProductProp({
            _id:_id,
            prodName:prodName,
            prodDesc:prodDesc,
            price:price,
            isAvailable:isAvailable,
            stockAmount:stockAmount,
            orders:orders
        })
    },[_id, prodName, prodDesc, price, isAvailable, stockAmount, orders]);



    //? this is for archive
    const handleArchiveClick = (e) => {
        fetch(`http://localhost:4000/products/${_id}/archive`,{
            method: 'PATCH',
            'Content-Type': 'application/json'
        })
        .then(res=>res.json())
        .then(data=>{
            (data)?
            Swal.fire({
                icon:'success',
                title: 'Archive status updated successfully',
                confirmButtonText: 'Reload page',
                allowOutsideClick: false,
            })
            .then
            (result=>{
                window.location.reload(false);
            })
            :
            Swal.fire({
                icon: 'error',
                title: 'Something went wrong',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    //? this is for restock
    const [inputStockAmount, setInputStockAmount] = useState(0);
    const handleRestockClick = (e) => {
        fetch(`http://localhost:4000/products/${_id}/restockProduct`,{
            method : 'PATCH',
            body: JSON.stringify({inputStockAmount}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(
            Swal.fire({
                icon:'success',
                title: `Added ${inputStockAmount} to ${prodName} stock`,
                allowOutsideClick: false,
                timer: 15000
            })
            .then(result=>(window.location.reload(false)))
        )
    }

    //?This is for WhoOrdered

    const handleWhoOrderedClick = (e) => {
        fetch(`http://localhost:4000/products/${_id}/listOrders`,{
            method : 'GET',
            headers: {
                'Content-Type': 'text/html',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res=>res.text())
        .then(data=>{
            console.log(data)
            Swal.fire({
                title: `List of Orders for <br/>${prodName}`,
                confirmButtonText: 'Close',
                allowOutsideClick: false,
                html: data
            })
        })
    }
    
    return (
        <Card className="m-auto" id="productsContainerCard">
            <Card.Body>
                <Card.Title>{prodName}</Card.Title>
                <Card.Subtitle className="text-muted">id: {_id}</Card.Subtitle>
                <Card.Text>{prodDesc}</Card.Text>
                <Card.Text>Price: ₱{price}</Card.Text>
                <Card.Text>Stock: {stockAmount}</Card.Text>
                <Card.Text>Is Available: {
                    (isAvailable)?
                    'Yes'
                    :
                    'No'
                    }</Card.Text>

                <div>
                    <EditProduct EditProductProp={EditProductProp}/>
                </div>
                

                <div><Button variant="warning" className="my-2" style={{width:'10rem'}} onClick={handleArchiveClick}>Toggle Availability</Button></div>
                
                <div className="input-group" style={{width:'10rem'}}>
                    <input
                    type="number"
                    value={inputStockAmount}
                    onChange={(e)=> setInputStockAmount(e.target.value)}
                    className="form-control"
                    placeholder='0'/>
                    <span className="input-group-append">
                        <Button variant="warning"
                        onClick={handleRestockClick}>
                        Restock
                        </Button>
                    </span>
                </div>
                <span>
                    <Button
                    variant="warning"
                    className="mt-2"
                    style={{width:'10rem'}}
                    onClick={handleWhoOrderedClick}
                    >Who ordered?
                    </Button>
                </span>

            </Card.Body>
        </Card>
    )
}

ProductsContainerCard.propTypes = {
    ProductsContainerProp: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        prodName: PropTypes.string.isRequired,
        prodDesc: PropTypes.string,
        price: PropTypes.number.isRequired,
        isAvailable: PropTypes.bool.isRequired,
        stockAmount: PropTypes.number.isRequired,
        orders: PropTypes.array.isRequired
    })
}