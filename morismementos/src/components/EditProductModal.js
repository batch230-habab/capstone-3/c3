import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2';

function EditProduct({EditProductProp}) {
    const {_id, prodName, prodDesc, price} = EditProductProp;
    const [inputProdName, setInputProdName] = useState(prodName),
    [inputProdDesc, setInputProdDesc] = useState(prodDesc),
    [inputPrice, setInputPrice] = useState(price);
    const [show, setShow] = useState(false);


    const handleClose = () => {
        setShow(false);
        window.location.reload();
    }
    const handleShow = () => setShow(true);


    const handleSubmit = (e) => {
        fetch(`http://localhost:4000/products/${_id}/update`,{
            method: 'POST',
            body: JSON.stringify({inputProdName, inputProdDesc, inputPrice}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            (data)?
            Swal.fire({
                title: "Product Updated Successfully",
                icon: "success",
                timer: 10000
            })
            :
            Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                timer: 10000
            })
        })
    }
    return (
    <>
        <Button variant="success" className="me-3" onClick={handleShow}>Edit</Button>

        <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        >
        <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <form className="w-full max-w-sm">
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                        Product Name:
                    </label>
                </div>
                <div className="md:w-2/3">
                    <input
                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    placeholder={inputProdName}
                    value={inputProdName}
                    onChange={(e) => setInputProdName(e.target.value)}
                    />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                        Description:
                    </label>
                </div>
                <div className="md:w-2/3">
                    <textarea
                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="textarea"
                    placeholder={inputProdDesc}
                    value={inputProdDesc}
                    onChange={(e) => setInputProdDesc(e.target.value)}
                    />
                </div>
            </div>
            <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                        Price: ₱
                    </label>
                </div>
                <div className="md:w-2/3">
                    <input
                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="number"
                    placeholder={inputPrice}
                    value={inputPrice}
                    onChange={(e) => setInputPrice(e.target.value)}
                    />
                </div>
            </div>
        </form>
        </Modal.Body>
        <Modal.Footer>
            <button
            className='bg-slate-400 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded' onClick={handleClose}>Close</button>
            <button
            className='bg-blue-400 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
            onClick = {handleSubmit}>Save changes</button>
        </Modal.Footer>
        </Modal>
    </>
    );
}

export default EditProduct;