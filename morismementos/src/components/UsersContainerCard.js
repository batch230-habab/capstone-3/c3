import { Fragment } from 'react';
import {useEffect, useState, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import './UsersContainerCard.css';


export default function UsersContainerCard({UsersContainerCardProp}){

    const {_id, firstName, lastName, email, mobileNo, isAdmin, orders} = UsersContainerCardProp;

    //?for Toggle Admin

    const [userId, setUserId] = useState(_id);
    const handleRemoveAdmin = (e) => {
        fetch(`http://localhost:4000/users/${userId}/removeAdmin`,{
            method: 'PATCH',
            'Content-Type': 'application/json',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.text())
        .then(data => {
            Swal.fire({
                title:data,
                allowOutsideClick: false,
                text: 'Reload page to apply changes',
                confirmButtonText: 'OK',
            })
            .then(()=>{window.location.reload(false)})
        }
        )}
        
    
    const handleMakeAdmin = (e) => {
        fetch(`http://localhost:4000/users/${userId}/setToAdmin`,{
            method: 'PATCH',
            'Content-Type': 'application/json',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.text())
        .then(data => {
            Swal.fire({
                title:data,
                allowOutsideClick: false,
                text: 'Reload page to apply changes',
                confirmButtonText: 'OK',
            })
            .then(()=>{window.location.reload(false)})
        }
        )
    }


    //?for Delete User

    const handleDeleteUser = (e) => {
        fetch(`http://localhost:4000/users/${userId}/deleteUser`,{
            method: 'DELETE',
            'Content-Type': 'application/json',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.text())
        .then(data => {
            Swal.fire({
                title: data,
                allowOutsideClick: false,
                text: 'Reload page to apply changes',
                confirmButtonText: 'OK',
            })
            .then(()=>{window.location.reload(false)})
            }
            )
    }
    return(
        <Card className="m-auto" id="usersContainerCard">
            <Card.Body>
                <Card.Title>{firstName} {lastName}</Card.Title>
                <Card.Subtitle className="text-muted">id: {_id}</Card.Subtitle>
                <Card.Text>Email: {email}</Card.Text>
                <Card.Text>Contact #: {mobileNo}</Card.Text>
                <Card.Text>Admin: {isAdmin.toString()}</Card.Text>
                {
                    (isAdmin)?(
                        <Button variant="secondary" className="m-2" onClick={handleRemoveAdmin}>Remove Admin</Button>
                    )
                    :
                    (
                        <Button variant="secondary" className="m-2" onClick={handleMakeAdmin}>Make Admin</Button>
                    )
                }
                    
                    <Button variant="danger" className="m-2" onClick={handleDeleteUser}>Delete User</Button>
            </Card.Body>
        </Card>
    )
}