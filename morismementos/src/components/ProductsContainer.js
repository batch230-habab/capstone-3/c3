import ProductsContainerCard from './ProductsContainerCard.js';
import React from 'react';
import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';

export default function ProductsContainer() {
    const [products, setProducts] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {
        fetch('http://localhost:4000/products/',{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product=>{
                return(
                    <ProductsContainerCard ProductsContainerCardProp={product} key={product._id}/>
                )
            }))
        })
    },[])
    return (
        <>{products}</>
    )
}