import UsersContainerCard from './UsersContainerCard.js';
import React from 'react';
import { Fragment } from 'react';
import {useEffect, useState, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function UsersContainer() {
    const [users, setUsers] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {
        fetch('http://localhost:4000/users',{
            headers:{
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => setUsers(data.map(user =>  {
            return(
                <UsersContainerCard UsersContainerCardProp={user} key={user._id} />
            )
        })));
    }, [])
    return(
        <>{users}</>
    )
}