import { React, useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { Input, button } from '@material-ui/core';
import Swal from 'sweetalert2';

export default function AddProduct(){
    const [prodName, setProdName] = useState('');
    const [prodDesc, setProdDesc] = useState('');
    const [price, setPrice] = useState('');
    const [stockAmount, setStockAmount] = useState('');

    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
        window.location.reload();
    }

    const handleSubmit = (e) =>{
        fetch('http://localhost:4000/products/newProduct', {
            method: 'POST',
            body: JSON.stringify({
                prodName: prodName,
                prodDesc: prodDesc,
                price: price,
                stockAmount: stockAmount
        }),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }})
        .then(res => res.json())
        .then(data => {
            (data)?
            Swal.fire({
            icon:'success',
            title: 'Product Added Successfully',
            timer: 1500
            })
            :
            Swal.fire({
                icon: 'error',
                title: 'Product Not Added',
                timer: 1500,
                text: 'Something went wrong'
            })
        })
    }



    const handleShow = () => {
        setShow(true);
    }

    return(
        <>
            <Button variant="primary" className="me-3" onClick={handleShow}>Add Product</Button>

            <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Add Product</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form className="w-full max-w-sm">
                        <div className="md:flex md:items-center mb-6">
                            <div className="md:w-1/3">
                                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                                    Product Name:
                                </label>
                            </div>
                            <div className="md:w-2/3">
                                <input
                                className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                type="text"
                                placeholder='Product name'
                                value={prodName}
                                onChange={(e) => setProdName(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="md:flex md:items-center mb-6">
                            <div className="md:w-1/3">
                                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                                    Description:
                                </label>
                            </div>
                            <div className="md:w-2/3">
                                <textarea
                                className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                type="textarea"
                                placeholder='Describe the product'
                                value={prodDesc}
                                onChange={(e) => setProdDesc(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="md:flex md:items-center mb-6">
                            <div className="md:w-1/3">
                                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                                    Price:
                                </label>
                            </div>
                            <div className="md:w-2/3">
                                <input
                                className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                type="number"
                                placeholder='00'
                                value={price}
                                onChange={(e) => setPrice(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="md:flex md:items-center mb-6">
                            <div className="md:w-1/3">
                                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                                    Stock Amount:
                                </label>
                            </div>
                            <div className="md:w-2/3">
                                <input
                                className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                type="number"
                                placeholder='Stock amount'
                                value={stockAmount}
                                onChange={(e) => setStockAmount(e.target.value)}
                                />
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                <button
                className='bg-slate-400 hover:bg-slate-700 text-white font-bold py-2 px-4 rounded'
                onClick={handleClose}>Close</button>
                <button
                className='bg-blue-400 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                onClick = {handleSubmit}>Add Product</button>
                </Modal.Footer>
            </Modal>
        </>
    )
}