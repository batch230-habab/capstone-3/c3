import React from 'react'
import{ useState, useEffect, useContext} from 'react';
import { Card, Button } from 'react-bootstrap';
import './css/viewCart.css';

export default function ViewCart(){

    let [orderData, setOrderData] = useState(null);
    fetch(`http://localhost:4000/users/cart`,{
        method: 'GET',
        headers: {
            'Content-Type': 'text/html',
            'Authorization': `Bearer ${localStorage.token}`
        }
    }).then(res => res.text())
    .then(data=> {
        setOrderData(data);
    })
    console.log(orderData);
    return(
        <div className="m-5" id="receiptData">
            <Card id="receiptDataCard" dangerouslySetInnerHTML={{__html: orderData}}>
            </Card>
            <Card.Footer>
                <Button variant="warning">
                    Proceed to payment
                </Button>
            </Card.Footer>
        </div>

        
    )
}