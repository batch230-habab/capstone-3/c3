import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

//? password handling dependencies
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import { VisibilityOffOutlined } from '@material-ui/icons';


export default function Login(){
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState(''),
    [password, setPassword] = useState(''),
    [isAdmin, setIsAdmin] = useState(false);

    //?password handling
    const [isActive, setIsActive] = useState(false);

    //?showpassword
    const [showPassword, setShowPassword] = useState(false);
    const showPasswordHandler = () => {setShowPassword(!showPassword)}

    function login(e){
        e.preventDefault();
        fetch('http://localhost:4000/users/login',{
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => res.json())
        .then(data => {
            console.log(data);
            if(data !== false){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Enjoy our handmade jewelry!"
                })
            }else{
                Swal.fire({
                    title: "Login Failed",
                    icon: "error",
                    text: "please check your credentials",
                    confirmButtonText: "OK"
                })
            }
        })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/userDetails',{
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                email: data.email,
                id: data._id,
                isAdmin: data.isAdmin,
                firstName: data.firstName
            })
            localStorage.setItem('id', data._id);
            localStorage.setItem('email', data.email);
            localStorage.setItem('isAdmin', data.isAdmin);
            localStorage.setItem('firstName', data.firstName);
        })
    }
    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    return(
        (user.id !== null)?
            <Navigate to='/'/>
            :
            <div className="justify-content-center container-fluid row d-flex">
                <Form onSubmit={(e) => login(e)}>
                    <h3>Login</h3>
                    <Form.Group controlId="userEmail">
                        <Input
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required/>
                    </Form.Group>
                    <Form.Group controlId="password" className="mb-3">
                        <Input
                        type={showPassword? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton onClick={showPasswordHandler}>
                                    {showPassword? <Visibility/> : <VisibilityOffOutlined/>}
                                </IconButton>
                            </InputAdornment>
                        }
                        onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                    {isActive?
                        <Button variant="primary" type="submit">Login</Button>
                        :
                        <Button variant="danger" type="submit" disabled>Required fields are empty</Button>
                    }
                    <p className="mt-3">Don't have an account? <Link to="/register">Register</Link></p>
                </Form>
            </div>
    )

}