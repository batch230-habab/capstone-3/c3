import ProductsCard from "../components/ProductsCard";
import React from 'react';
import { Fragment } from 'react';
import {useEffect, useState, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import {Row} from 'react-bootstrap';

export default function Products(){
    const {user} = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(()=>{
        fetch('http://localhost:4000/products/availableProducts',{
            headers:{
                'Authorization':`Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            setProducts(data.map(product=>{
                return (
                    <ProductsCard ProductsProp={product} key={product._id}/>
                )
            }))
        })
    },[])
    return(
        <Row className="d-flex">
            {products}
        </Row>
    )
    
}