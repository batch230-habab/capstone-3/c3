import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function Register(){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');

    const [isActive, setIsActive] = useState(false);

    const { user } = useContext(UserContext);

    function registerUser(e) {
        e.preventDefault();
        fetch('http://localhost:4000/users/register', {
            method: 'POST',
            body: JSON.stringify({firstName, lastName, email, password, mobileNo}),
            headers: {'Content-Type': 'application/json'}
        })
        .then(result => result.json())
        .then(data=>{
            console.warn(data);
            if(data === false){
                Swal.fire({
                    title: "Registration Failed!",
                    icon: "error",
                    text: 'Email/Mobile Number is already registered!'
                })
            }else{
            Swal.fire({
                title: "Registration Successful!",
                text: "Please Login to continue",
                timer: 20000
            })
            .then(res=>(window.location.href='/login'))
            }
        })
        
    }
    useEffect(() => {
        if((email!==''&&
        password!==''&&
        confirmPassword!==''&&
        firstName!==''&&
        lastName!==''&&
        mobileNo!==''&&
        (password === confirmPassword))){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password, confirmPassword, firstName, lastName, mobileNo])

    return(
        (user.id === null)?
        <Container className="py-5 col-4">
            <Form onSubmit={(e)=>registerUser(e)} className="p-5">
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={(e)=>setEmail(e.target.value)}
                            required
                            />
                            <Form.Text className="text-muted">
                                Email must be unique.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={(e)=>setPassword(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group controlId="confirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                            type="password"
                            placeholder="Confirm Password"
                            value={confirmPassword}
                            onChange={(e)=>setConfirmPassword(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                            type="text"
                            placeholder="First Name"
                            value={firstName}
                            onChange={(e)=>setFirstName(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                            type="text"
                            placeholder="Last Name"
                            value={lastName}
                            onChange={(e)=>setLastName(e.target.value)}
                            required
                            />
                        </Form.Group>

                        <Form.Group controlId="mobileNo" className="mb-5">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                            type="text"
                            placeholder="Mobile Number"
                            value={mobileNo}
                            onChange={(e)=>setMobileNo(e.target.value)}
                            required
                            />
                    </Form.Group>
                    {isActive?
                        <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                        :
                        <Button variant="danger" type="submit" id="submitBtn" disabled>Required fields are not satisfied</Button>
                    }
            </Form>
        </Container>
        
    :
    <Navigate to="/products"/>
    )
}